// constructor function called EventObserver
function EventObserver() {
  // the functions that will be passed into it
  this.observers = [];
}

EventObserver.prototype = {
  subscribe: function(fn) {
    this.observers.push(fn);
    console.log(`You are now subscribed to ${fn.name}`);
  },

  unsubscribe: function(fn) {
    /* Filter out from the list whatever matches the callback function.
       If there is no match, the callback gets to stay on the list. 
       The filter returns a new list and reassigns the list of observers.
    */
    this.observers = this.observers.filter( (item) => {
      if (item !== fn) {
        return item;
      }
    });
    console.log(`You are now unsubscribed from ${fn.name}`);
  },

  fire: function() {
    this.observers.forEach(element => {
      element.call();
    });
  }
}


// test
const click = new EventObserver();

// Event listeners

// milliseconds
document.querySelector('.sub-ms').addEventListener( 'click', () => {
  click.subscribe(getCurMilliseconds);
});

document.querySelector('.unsub-ms').addEventListener( 'click', () => {
  click.unsubscribe(getCurMilliseconds);
});

// seconds
document.querySelector('.sub-s').addEventListener( 'click', () => {
  click.subscribe(getCurSeconds);
});

document.querySelector('.unsub-s').addEventListener( 'click', () => {
  click.unsubscribe(getCurSeconds);
});

// fire
document.querySelector('.fire').addEventListener( 'click', () => {
  click.fire();
});

// click handler
const getCurMilliseconds = function() {
  console.log(`Current Milliseconds: ${new Date().getMilliseconds()}`);
}

const getCurSeconds = function() {
  console.log(`Current Seconds: ${new Date().getSeconds()}`);
}