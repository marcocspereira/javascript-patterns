
class EventObserverES6 {

  constructor() {
    // the functions that will be passed into it
    this.observers = [];
  }

  subscribe(fn) {
    this.observers.push(fn);
    console.log(`(ES6) You are now subscribed to ${fn.name}`);
  };

  unsubscribe(fn) {
    /* Filter out from the list whatever matches the callback function.
       If there is no match, the callback gets to stay on the list. 
       The filter returns a new list and reassigns the list of observers.
    */
    this.observers = this.observers.filter( (item) => {
      if (item !== fn) {
        return item;
      }
    });
    console.log(`(ES6) You are now unsubscribed from ${fn.name}`);
  };

  fire() {
    this.observers.forEach(element => {
      element.call();
    });
  }
}



// test
const clickES6 = new EventObserverES6();

// Event listeners

// milliseconds
document.querySelector('.sub-ms').addEventListener( 'click', () => {
  clickES6.subscribe(getCurMillisecondsES6);
});

document.querySelector('.unsub-ms').addEventListener( 'click', () => {
  clickES6.unsubscribe(getCurMillisecondsES6);
});

// seconds
document.querySelector('.sub-s').addEventListener( 'click', () => {
  clickES6.subscribe(getCurSecondsES6);
});

document.querySelector('.unsub-s').addEventListener( 'click', () => {
  clickES6.unsubscribe(getCurSecondsES6);
});

// fire
document.querySelector('.fire').addEventListener( 'click', () => {
  clickES6.fire();
});

// click handler
const getCurMillisecondsES6 = function() {
  console.log(`(ES6) Current Milliseconds: ${new Date().getMilliseconds()}`);
}

const getCurSecondsES6 = function() {
  console.log(`(ES6) Current Seconds: ${new Date().getSeconds()}`);
}