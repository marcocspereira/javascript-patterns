# Observer Pattern

It allows to **subscribe** and **unsubscribe** to certain events or certain functionality. It gives a nice *subscription model*.

It can be used to **notify the DOM** of certain elements to be updated.

For this pattern we will use an example with **prototypes**, at first, and then another example using **ES6 classes**. It includes the creation of some buttons to subscribe and unsubscribe to certain events. 

## With Prototypes
```javascript
// constructor function called EventObserver
function EventObserver() {
  // the functions that will be passed into it
  this.observers = [];
}

EventObserver.prototype = {
  subscribe: function(fn) {
    this.observers.push(fn);
    console.log(`You are now subscribed to ${fn.name}`);
  },

  unsubscribe: function(fn) {
    /* Filter out from the list whatever matches the callback function.
       If there is no match, the callback gets to stay on the list. 
       The filter returns a new list and reassigns the list of observers.
    */
    this.observers = this.observers.filter( (item) => {
      if (item !== fn) {
        return item;
      }
    });
    console.log(`You are now unsubscribed from ${fn.name}`);
  },

  fire: function() {
    this.observers.forEach(element => {
      element.call();
    });
  }
}


// test
const click = new EventObserver();

// Event listeners

// milliseconds
document.querySelector('.sub-ms').addEventListener( 'click', () => {
  click.subscribe(getCurMilliseconds);
});

document.querySelector('.unsub-ms').addEventListener( 'click', () => {
  click.unsubscribe(getCurMilliseconds);
});

// seconds
document.querySelector('.sub-s').addEventListener( 'click', () => {
  click.subscribe(getCurSeconds);
});

document.querySelector('.unsub-s').addEventListener( 'click', () => {
  click.unsubscribe(getCurSeconds);
});

// fire
document.querySelector('.fire').addEventListener( 'click', () => {
  click.fire();
});

// click handler
const getCurMilliseconds = function() {
  console.log(`Current Milliseconds: ${new Date().getMilliseconds()}`);
}

const getCurSeconds = function() {
  console.log(`Current Seconds: ${new Date().getSeconds()}`);
}
```

## With ES6 Classes
```javascript

class EventObserverES6 {

  constructor() {
    // the functions that will be passed into it
    this.observers = [];
  }

  subscribe(fn) {
    this.observers.push(fn);
    console.log(`(ES6) You are now subscribed to ${fn.name}`);
  };

  unsubscribe(fn) {
    /* Filter out from the list whatever matches the callback function.
       If there is no match, the callback gets to stay on the list. 
       The filter returns a new list and reassigns the list of observers.
    */
    this.observers = this.observers.filter( (item) => {
      if (item !== fn) {
        return item;
      }
    });
    console.log(`(ES6) You are now unsubscribed from ${fn.name}`);
  };

  fire() {
    this.observers.forEach(element => {
      element.call();
    });
  }
}

// test
const clickES6 = new EventObserverES6();

// Event listeners

// milliseconds
document.querySelector('.sub-ms').addEventListener( 'click', () => {
  clickES6.subscribe(getCurMillisecondsES6);
});

document.querySelector('.unsub-ms').addEventListener( 'click', () => {
  clickES6.unsubscribe(getCurMillisecondsES6);
});

// seconds
document.querySelector('.sub-s').addEventListener( 'click', () => {
  clickES6.subscribe(getCurSecondsES6);
});

document.querySelector('.unsub-s').addEventListener( 'click', () => {
  clickES6.unsubscribe(getCurSecondsES6);
});

// fire
document.querySelector('.fire').addEventListener( 'click', () => {
  clickES6.fire();
});

// click handler
const getCurMillisecondsES6 = function() {
  console.log(`(ES6) Current Milliseconds: ${new Date().getMilliseconds()}`);
}

const getCurSecondsES6 = function() {
  console.log(`(ES6) Current Seconds: ${new Date().getSeconds()}`);
}
```