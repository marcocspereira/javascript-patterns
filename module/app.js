// Basic structure

/* // a module that is a IIFE
(function() {
  // declare PRIVATE variables and functions

  return {
    // declare PUBLIC variables and functions
  }
})(); */

// STANDARD MODULE PATTERN
const UICtrl = (function() {
  
  // PRIVATE
  let text = 'Hello World';

  const changeText = function() {
    const element = document.querySelector('h1');
    element.textContent = text;
  }

  return {
    // PUBLIC
    callChangeText: function() {
      changeText();
      console.log(text);
    }
  }
})();

UICtrl.callChangeText();

// REVEAL MODULE PATTERN
const ItemCrl = (function() {

  // PRIVATE
  // the state
  let data = [];

  function add(item) {
    data.push(item);
    console.log('Item added...  ');
  }

  function get(id) {
    return data.find(item => item.id === id);
  }

  // PUBLIC
  return {
    add: add,
    get: get
  }
})()

ItemCrl.add({id:1, name: 'Rocha'});
ItemCrl.add({id:2, name: 'Marco'});
ItemCrl.add({id:3, name: 'Amaral'});
ItemCrl.add({id:4, name: 'Miguel'});
console.log(ItemCrl.get(1));