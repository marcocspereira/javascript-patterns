# Module Pattern

ES6 / ES2015 introduced modules in JavaScript, which means that we can use separate files to export modules and import in a new file. However, this is still not supported in browsers, so you'll have to use a compiler like **Babel** along with a module loader like **Webpack**.

ES5 has the **Module Pattern** that allow us to break parts of our code into self-contain modules with private properties and methods. 

```javascript
// STANDARD MODULE PATTERN
const UICtrl = (function() {
  
  // PRIVATE
  let text = 'Hello World';

  const changeText = function() {
    const element = document.querySelector('h1');
    element.textContent = text;
  }

  return {
    // PUBLIC
    callChangeText: function() {
      changeText();
      console.log(text);
    }
  }
})();

// testing
UICtrl.callChangeText();
```

# Reveal Module Pattern

It is very similar. The **main difference** is that, instead of returning our own public functions, we basically map an object literal to private functions that you want to reveal.

```javascript
// REVEAL MODULE PATTERN
const ItemCrl = (function() {

  // PRIVATE
  // the state
  let data = [];

  function add(item) {
    data.push(item);
    console.log('Item added...');
  }

  function get(id) {
    return data.find(item => item.id === id);
  }

  // PUBLIC
  return {
    add: add,
    get: get
  }
})()

// testing
ItemCrl.add({id:1, name: 'Rocha'});
ItemCrl.add({id:2, name: 'Marco'});
ItemCrl.add({id:3, name: 'Amaral'});
ItemCrl.add({id:4, name: 'Miguel'});
console.log(ItemCrl.get(1));
```

Often you can see an **_** to describe **private** properties or methods.

```javascript
// example
let _data = [];
```