const Singleton = (function() {

  let instance;

  function createInstance() {
    const object = new Object('Object instance');
    return object;
  }

  return {
    getInstance: function() {
      if(!instance) {
        instance = createInstance();
      }
      return instance;
    }
  }

})();

const instanceA = Singleton.getInstance();
const instanceB = Singleton.getInstance();

console.log(instanceA);
// it gives the same instance because you can't have more than one instance with this pattern
console.log(instanceA === instanceB); 