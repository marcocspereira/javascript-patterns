# Singleton Pattern

It is a kind of manifestation of the Module Pattern. A Singleton object is an IIFE and it can only return one instance of an object at time.

Like the Module Pattern, it mantains the private reference, which nothing from the outside can access.

## Example to use Singleton Pattern

* One user object created at the time, maybe a logged in user which prevents for having two users for being created at once.

```javascript
const Singleton = (function() {

  let instance;

  function createInstance() {
    const object = new Object('Object instance');
    return object;
  }

  return {
    getInstance: function() {
      if(!instance) {
        instance = createInstance();
      }
      return instance;
    }
  }

})();

// testing
const instanceA = Singleton.getInstance();
const instanceB = Singleton.getInstance();

console.log(instanceA);

// it gives the same instance because you can't have more than one instance with this pattern
console.log(instanceA === instanceB); 
```