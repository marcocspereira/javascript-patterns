# JavaScript Patterns

* Reusable solution that can be applied to occurring problems in software design;

* Can also be thought of as programming templates;

* Situations vary significantly.

---

# Patterns

* Module & Revealing Module

* Singleton

* Factory

* Observer

* Mediator

* State