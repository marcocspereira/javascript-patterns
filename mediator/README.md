# Mediator Pattern

It is another **behaviour pattern** ([Wikipedia description](https://en.wikipedia.org/wiki/Mediator_pattern)), like the *Observer*. The idea is to have a mediator, which is basicaly an interface for communicating with what are called **colleagues**, which are mediator objects.

## Example to use Mediator Pattern

A **chat room**. We use two *Constructor functions*:

* one for the **user**;

* one for the **chat room**.

```javascript
const User = function(name) {
  this.name = name;
  this.chatroom = null;
}

User.prototype = {

  send: function(message, to) {
    this.chatroom.send(message, this, to);
  },

  receive: function(message, from) {
    console.log(`${from.name} to ${this.name}: ${message}`);
  }
}

const Chatroom = function() {
  let users = {}; // list of users

  return {
    register: function(user) {
      users[user.name] = user;
      user.chatroom = this; // the current chat room
    },

    send: function(message, from, to) {
      if (to) {
        // single user message
        to.receive(message, from);
      }
      else {
        // mass message
        for(key in users) {
          if(users[key] !== from) {
            users[key].receive(message, from);
          }
        }
      }
    }
  }
}


// testing

const marco = new User('Marco');
const amaral = new User('Amaral');
const miguel = new User('Miguel');
const rocha = new User('Rocha');

const chatroom = new Chatroom();

chatroom.register(marco);
chatroom.register(amaral);
chatroom.register(miguel);
chatroom.register(rocha);

marco.send('Hello Amaral', amaral);
miguel.send('Hello Rocha', rocha);
rocha.send('Hello everyone!!!');
```