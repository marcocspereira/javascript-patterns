# Factory Pattern

Also called the **Factory method**, which is a way of creating **interfaces** for creating objects, but we can let sub-classes to define which classes to instatiate. 

Factory methods are often used in applications to manage, mantain and manipulate collections of objects that are different but at the sime time have many common characteristics

# Example to use Factory Pattern

A **member**. Some kind of paid membership of application or website where your members have different types, but still have same properties and methods.

```javascript
function MemberFactory() {
  this.createMember = function(name, type) {
    let member;

    if (type === 'simple') {
      member = new SimpleMembership(name);
    }
    else if (type === 'standard') {
      member = new StandardMembership(name);
    }
    else if (type === 'super') {
      member = new SuperMembership(name);
    }

    member.type = type;

    member.define = function() {
      console.log(`${this.name} (${this.type}): ${this.cost}`);
    }

    return member;
  }
}


const SimpleMembership = function(name) {
  this.name = name;
  this.cost = '$5';
}

const StandardMembership = function(name) {
  this.name = name;
  this.cost = '$15';
}

const SuperMembership = function(name) {
  this.name = name;
  this.cost = '$25';
}

// testing
const members = [];
const factory = new MemberFactory();

members.push(factory.createMember('Marco', 'simple'));
members.push(factory.createMember('Amaral', 'standard'));
members.push(factory.createMember('Miguel', 'super'));

console.log(members);

members.forEach( (member) => member.define());
```